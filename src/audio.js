// original implementation taken from shamelessly grabbed from https://hostrider.com/

// load the audio at teh start
const cat_angry = new Audio("./sounds/cat-angry.mp3");
const cat_menu = new Audio("./sounds/cat-menu.mp3");
const cat_meow = new Audio("./sounds/cat-meow.mp3");

// listen for events, this will prob drive folks catty
document.addEventListener('touchstart', keyPlay);
document.addEventListener('click', keyPlay);
document.addEventListener('keydown', keyPlay);

// play a random audio file
function keyPlay() {
  // will randomly play 1 of 3 sounds on touch click or keypress
  switch (Math.floor(Math.random() * 3)){
    case 0: {
      cat_angry.play();
      break;
    }
    case 1: {
      cat_menu.play();
      break;
    }
    case 2: {
      cat_meow.play();
      break;
    }
    default: {
      cat_meow.play();
    }
  }

}